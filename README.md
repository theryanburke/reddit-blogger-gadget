# Reddit Gadget for Google Blogger

Some code below

```
 {
   "key": {
     "key2" : 1,
	 "key3" : "string"
	}
 }
```

And a table:

Section  | Description
------------- | ------------- 
[How to Post/Edit Documents](InternalDocsDocumentation) | This is a step-by-step documentation on the process for posting items in Github, via SourceTree
[Legal](Legal) | Contains documents for legal usage by 2lemtery (i.e. licensing agreements, etc.)
[Marketing](Marketing) | This Folder contains all things related to 2lemetry Marketing (i.e. Blog procedures). 
[Organizational](Organizational)  | Corporate information, org charts, functional team assignments
[Projects](Projects) | This folder contains all documentation and screen shots for current projects.  
[Technical](Technical) | For coding standards and Hubot coding commands
[Tools](Tools) | This folder contains how-to procedures for internal online tools, such as Github and Jira. 
[Orientation](orientation.md) | Details employee policies. New hires start here!